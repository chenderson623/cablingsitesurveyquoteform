//Gruntfile
module.exports = function(grunt) {
    'use strict';

    require('time-grunt')(grunt);
    var modRewrite = require('connect-modrewrite');
    var serveStatic = require('serve-static');
    var serveIndex = require('serve-index');

    //
    //Initializing the configuration object
    //-------------------------------------------------------------------------
    grunt.initConfig({
        config: {
            src: 'src',
            build: 'build',
            dist: 'dist'
        },
        //
        // Task configuration
        //---------------------------------------------------------------------
        clean: {
            options: {
                force: false
            },
            build: ['<%= config.build %>/css/*', '<%= config.build %>/js/*', '<%= config.build %>/vendor/*', '<%= config.build %>/images/*', '<%= config.build %>/fonts/*'],
            dist: ['<%= config.dist %>/js/*']
        },
        copy: {
            build: {
                files: [
                    // includes files within path
                    // javascript files:
                    {
                        expand: true,
                        flatten: true,
                        src: [
                            '<%= config.src %>/bower_components/jquery/dist/jquery.min.js',
                            '<%= config.src %>/bower_components/requirejs/require.js',
                            '<%= config.src %>/bower_components/bootstrap/dist/js/bootstrap.min.js'
                        ],
                        dest: '<%= config.build %>/vendor',
                        filter: 'isFile'
                    },
                ]
            }
        },
        concat: {
            options: {
                stripBanners: false
            },
            vendor: {
                src: [
                    '<%= config.src %>/bower_components/underscore/underscore-min.js',
                    '<%= config.src %>/bower_components/backbone/backbone-min.js',
                    '<%= config.src %>/bower_components/backbone.localStorage/backbone.localStorage-min.js',
                ],
                dest: '<%= config.dist %>/js/vendor.js'
            },
        },

        jasmine: {
            development: {
                src : '<%= config.src %>/js/**/*.js', // will be using requirejs
                options : {
                    specs : '<%= config.src %>/tests/spec/**/*.spec.js',
                    outfile: '<%= config.src %>/tests/_SpecRunner.html',
                    keepRunner : true,
                    host: 'http://127.0.0.1:8000/',
                    template: require('grunt-template-jasmine-requirejs'),
                    templateOptions: {
                        requireConfigFile: '<%= config.src %>/js/require-config.js',
                        requireConfig: {
                            baseUrl: '/js',
                        },
                    }

                }
            }
        },

        //
        // This creates files for the build/js folder
        // ---------------------------------------------
        requirejs: {
            compile: {
                options: {
                    appDir: '<%= config.src %>/js',
                    baseUrl: './',
                    dir: '<%= config.build %>/js',
                    mainConfigFile: '<%= config.src %>/js/require-config.js',
                    optimize: 'none',
                    generateSourceMaps: true,
                    findNestedDependencies: true,
                    removeCombined: true,
                    // /build/js/vendor.js  - Need to manually add the entries from /src/js/require-config.js that we will need
                    modules: [{
                        name: 'vendor',
                        create: true,
                        include: [
                            'underscore',
                            'backbone',
                            'text',
                            'datatables.net',
                            'datatables.net-bs',
                            'jquery.showLoading',
                            'localstorage',
                            'bootstrap-datepicker',
                            'select2'
                        ]
                    },
                    // /build/js/app.js
                    {
                        name: 'app',
                        exclude: ['vendor'],
                        include: [
                            // Include anything common here. These will also need to be added to globalModules in amdclean
                            'common/TabNavModule'
                        ],
                    },
                    ],
                    done: function(done, output) {
                        var duplicates = require('rjs-build-analysis').duplicates(output);
                        if (Object.keys(duplicates).length) {
                            //grunt.log.subhead('Duplicates found in requirejs build:');
                            //grunt.log.warn(duplicates);
                            //return done(new Error('r.js built duplicate modules, please check the excludes option.'));
                        }
                        done();
                    }
                }
            }
        },


        connect: {
            options: {
                port: 9001,
                livereload: 35729,
                // Change this to '0.0.0.0' to access the server from outside
                hostname: '192.168.25.37',
                middleware: function(connect, options) {
                    var middlewares = [];
                    // RewriteRules support
                    middlewares.push(modRewrite(['^[^\\.]*$ /index.html [L]']));
                    if (!Array.isArray(options.base)) {
                        options.base = [options.base];
                    }
                    var directory = options.directory || options.base[options.base.length - 1];
                    options.base.forEach(function(base) {
                        // Serve static files.
                        middlewares.push(serveStatic(base));
                    });
                    // Make directory browse-able.
                    middlewares.push(serveIndex(directory));
                    return middlewares;

                }
            },
            test: {
                options: {
                    livereload: true,
                    base: ['src'],
                    open: {
                        target: 'tests/SpecRunner.html'
                    }
                }
            },
            src: {
                options: {
                    livereload: true,
                    base: ['src', 'frontend-src', 'public']
                }
            },
            backbone_forms: {
                options: {
                    livereload: true,
                    base: ['src/bower_components/backbone-base-and-form-view']
                }
            },
            build: {
                options: {
                    livereload: true,
                    base: ['build', 'src/bower_components']
                }
            },
            dist: {
                options: {
                    livereload: true,
                    base: ['', 'dist']
                }
            }
        },
        watch: {
            src: {
                files: ['Gruntfile.js', '<%= config.src %>/js/**/*.js', '<%= config.src %>/*.html', '<%= config.src %>/templates/**/*.html'],
                options: {
                    livereload: true //reloads the browser
                }
            },
            test: {
                files: ['Gruntfile.js', '<%= config.src %>/js/**/*.js', '<%= config.src %>/tests/**/*.js', '<%= config.src %>/tests/**/*.html'],
                options: {
                    livereload: true //reloads the browser
                }
            },
            build: {
                files: ['Gruntfile.js', '<%= config.src %>/js/**/*.js'],
                tasks: ['build'],
                options: {
                    livereload: true //reloads the browser
                }
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            files: [
                'Gruntfile.js',
                '<%= config.src %>/js/**/*.js',
                // Exclude files:
                //'!<%= config.src.js %>/libs-try/**/*.js'
            ]
        },

        complexity: {
            app: {
                //src: ['<%= config.src.js %>/**/*.js'],
                src: ['<%= config.src %>/js/common/**/*.js'],
                exclude: ['<%= config.src %>/js/libs-try/**/*.js'],
                options: {
                    breakOnErrors: true,
                    jsLintXML: 'complexity-report.xml', // create XML JSLint-like report
                    checkstyleXML: 'complexity-checkstyle.xml', // create checkstyle report
                    errorsOnly: false, // show only maintainability errors
                    cyclomatic: [3, 7, 12], // or optionally a single value, like 3
                    halstead: [8, 13, 20], // or optionally a single value, like 8
                    maintainability: 100,
                    hideComplexFunctions: false, // only display maintainability
                    broadcast: false // broadcast data over event-bus
                }
            }
        },

    });
    //
    // Plugin loading
    //-------------------------------------------------------------------------
    // These plugins provide necessary tasks.
    require('load-grunt-tasks')(grunt, {
        scope: 'devDependencies'
    });
    //
    // Task definition
    //-------------------------------------------------------------------------
    grunt.registerTask('install', ['shell:npm', 'shell:bower']);
    grunt.registerTask('serve', function(target) {
        if (target === 'dist') {
            //return grunt.task.run(['build', 'connect:dist:keepalive']);
            return grunt.task.run(['build', 'connect:dist', 'watch:build']);
        }
        if (target === 'build') {
            return grunt.task.run(['build', 'connect:build', 'watch:build']);
        }
        if (target === 'test') {
            return grunt.task.run(['connect:test', 'watch:test']);
        }
        grunt.task.run(['connect:src', 'watch:src']);
    });
    grunt.registerTask('build', ['jshint', 'clean', 'copy', 'requirejs', 'concat']);
};