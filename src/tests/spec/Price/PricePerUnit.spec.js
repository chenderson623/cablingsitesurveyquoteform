define([
    'Price/PricePerUnit',
    'UnitsOfMeasure/UnitQty',
    'UnitsOfMeasure/Unit'
], function(
    PricePerUnit,
    UnitQty,
    Unit
){
    'use strict';

    describe('PricePerUnit', function(){

        var unit_definition =  {
            unit    : 'foot',
            plural  : 'feet',
            abbrev  : 'ft',
            other   : []
        };

        var unit_qty;

        beforeEach(function(){
            unit_qty = UnitQty(Unit(unit_definition), 1);
        });

        it('is a function', function(){
            expect(typeof PricePerUnit).toEqual('function');
        });

        it('returns an object', function(){
            expect(typeof PricePerUnit(10, unit_qty)).toEqual('object');
        });

        it('produces a string', function(){
            expect(PricePerUnit(10, unit_qty).string()).toEqual('$10.00 per ft');
        });

        it('produces a unit value', function(){
            expect(PricePerUnit(10, UnitQty(Unit(unit_definition), 10)).unitValue()).toEqual(10);
        });

        it('produces a unit price', function(){
            expect(PricePerUnit(10, UnitQty(Unit(unit_definition), 10)).unitPrice()).toEqual(1);
        });

    });

});