define([
    'Price/Price',
    'Price/LinePrice',
    'UnitsOfMeasure/Length'
], function(
    Price,
    LinePrice,
    Length
){
    'use strict';

    describe('LinePrice', function(){


        it('returns an object', function(){
            expect(typeof LinePrice(Price(10).perLength('foot'))).toEqual('object');
        });
        it('returns an object', function(){
            expect(typeof LinePrice(Price(10).perLength('foot'), Length(10)('feet'))).toEqual('object');
        });
        it('produces a convertedUnitQty object', function(){
            var line_price = LinePrice(Price(10).perLength('foot'), Length(120)('inch'));
            expect(typeof line_price.convertedUnitQty()).toEqual('object');
        });
        it('produces a convertedUnitQty value', function(){
            var line_price = LinePrice(Price(10).perLength('foot'), Length(120)('inch'));
            expect(line_price.convertedUnitQty().value()).toEqual(10);
        });
        it('produces a price value', function(){
            expect(LinePrice(Price(10).perLength('foot'), Length(10)('feet')).price()).toEqual(100);
        });
        it('produces a converted price value', function(){
            expect(LinePrice(Price(10).perLength('foot'), Length(100)('inch')).price().toFixed(2)).toEqual('83.33');
        });

        it('triggers a change event', function(){
            var line_price = LinePrice(Price(10).perLength('foot'), Length(120)('inch'));

            var spys = {
                linePriceChanged: function() {
                    console.log("linePriceChanged", arguments);
                }
            };

            spyOn(spys, 'linePriceChanged').and.callThrough();

            line_price.on('change', spys.linePriceChanged);
            line_price.setQty(100);

            expect(spys.linePriceChanged).toHaveBeenCalled();

        });

    });

});