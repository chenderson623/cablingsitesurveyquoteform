define([
    'Price/Price'
], function(
    Price
){
    'use strict';

    describe('PricePerUnit', function(){


        it('returns an object', function(){
            expect(typeof Price(10)).toEqual('object');
        });

        it('produces a PricePerUnit object', function(){
            expect(Price(10).perLength('foot').string()).toEqual('$10.00 per ft');
        });

        it('produces a PricePerUnit object, with unit length', function(){
            expect(Price(10).perLength(100, 'foot').string()).toEqual('$10.00 per 100 ft');
        });

        it('produces a PricePerUnit object, as per foot', function(){
            expect(Price(10).perFoot().string()).toEqual('$10.00 per ft');
        });

    });

});