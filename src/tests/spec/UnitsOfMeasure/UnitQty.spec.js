define([
    'UnitsOfMeasure/UnitQty',
    'UnitsOfMeasure/Unit'
], function(
    UnitQty,
    Unit
){
    'use strict';

    describe('UnitQty', function(){

        var unit_definition =  {
            unit    : 'foot',
            plural  : 'feet',
            abbrev  : 'ft',
            other   : []
        };

        var unit_qty;

        beforeEach(function(){
            unit_qty = UnitQty(Unit(unit_definition), 100);
        });

        it('is an object', function(){
            expect(typeof unit_qty).toEqual('object');
        });

        it('can format value as string', function(){
            expect(unit_qty.string('plural')).toEqual('100 feet');
            expect(unit_qty.string('unit')).toEqual('100 foot');
            expect(unit_qty.string('abbrev')).toEqual('100 ft');
        });

        it('can tell unit from plural', function(){
            expect(unit_qty.string()).toEqual('100 feet');

            var unit_qty_unit = UnitQty(Unit(unit_definition), 1);
            expect(unit_qty_unit.string()).toEqual('1 foot');
        });

        it('returns abbrev', function(){
            expect(unit_qty.abbrev()).toEqual('100 ft');
        });

        it('returns value', function(){
            expect(unit_qty.value()).toEqual(100);
        });

        it('returns unit', function(){
            expect(unit_qty.unitString()).toEqual('foot');
        });

        it('returns unitDefinition', function(){
            expect(typeof unit_qty.unit()).toEqual('object');
        });

    });

});