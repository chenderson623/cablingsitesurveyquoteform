define([
    'UnitsOfMeasure/Length'
], function(
    Length
){
    'use strict';

    describe('Length', function(){

        it('is a function', function(){
            expect(typeof Length).toEqual('function');
        });

        it('returns a function', function(){
            expect(typeof Length(100)).toEqual('function');
        });

        it('can return a UnitQty', function(){
            expect(typeof Length(100)('feet')).toEqual('object');
            expect(typeof Length(100)('feet').string).toEqual('function');
        });

        it('can return a UnitQty that returns string', function(){
            expect(Length(100)('feet').string()).toEqual('100 feet');
        });

        it('has convertTo function', function(){
            expect(typeof Length(100)('feet').convertTo).toEqual('function');
        });

        it('can convert to other units', function(){
            expect(Length(100)('feet').convertTo('inches').string()).toEqual('1200 inches');
        });

    });

});