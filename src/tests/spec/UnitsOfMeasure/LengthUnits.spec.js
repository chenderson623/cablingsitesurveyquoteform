define([
    'UnitsOfMeasure/LengthUnits'
], function(
    LengthUnits
){
    'use strict';

    describe('LengthUnits', function(){

        it('is an object', function(){
            expect(typeof LengthUnits).toEqual('object');
        });

        it('can access unit definition matches', function(){
            expect(LengthUnits.get('feet').property('plural')).toEqual('feet');
        });

        it('has UnitConversionCollection', function(){
            var unit_conversion_collection = LengthUnits.conversions;
            expect(typeof unit_conversion_collection.findConversion).toEqual('function');
        });

    });

});