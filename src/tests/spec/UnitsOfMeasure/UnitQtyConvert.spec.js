define([
    'UnitsOfMeasure/UnitQtyConvert',
    'UnitsOfMeasure/UnitQty',
    'UnitsOfMeasure/UnitConversionCollection',
    'UnitsOfMeasure/UnitCollection'
], function(
    UnitQtyConvert,
    UnitQty,
    UnitConversionCollection,
    UnitCollection
){
    'use strict';

    describe('UnitQtyConvert', function(){

        var conversions = [
            [12 , 'inch'  , 1          , 'foot'],
            [1  , 'yard'  , 3          , 'foot'],
            [1  , 'mile'  , 5280       , 'foot'],
            [1  , 'meter' , 3.28083989 , 'foot']
        ];

        var unit_definitions = {
            'foot' : {
                'unit'    : 'foot',
                'plural'  : 'feet',
                'abbrev'  : 'ft',
                'other'   : []
            },
            'inch' : {
                'unit'    : 'inch',
                'plural'  : 'inches',
                'abbrev'  : 'in',
                'other'   : []
            },
            'yard' : {
                'unit'    : 'yard',
                'plural'  : 'yards',
                'abbrev'  : 'yd',
                'other'   : []
            },
            'mile' : {
                'unit'    : 'mile',
                'plural'  : 'miles',
                'abbrev'  : 'mi',
                'other'   : []
            },
            'meter' : {
                'unit'    : 'meter',
                'plural'  : 'meters',
                'abbrev'  : 'm',
                'other'   : ['mtr']
            }
        };

        var unit_collection;
        var unit_conversion_collection;

        beforeEach(function(){
            unit_collection            = UnitCollection(unit_definitions);
            unit_conversion_collection = UnitConversionCollection(unit_collection, conversions);
        });

        it('is a function', function(){
            expect(typeof UnitQtyConvert).toEqual('function');
        });

        it('returns an object', function(){
            var feet     = unit_collection.get('feet');
            var unit_qty = UnitQty(feet, 100);
            expect(typeof UnitQtyConvert(unit_qty, unit_conversion_collection)).toEqual('object');
        });

        it('returns an UnitQty object', function(){
            var feet             = unit_collection.get('feet');
            var unit_qty         = UnitQty(feet, 100);
            var unit_qty_convert = UnitQtyConvert(unit_qty, unit_conversion_collection);
            var inches           = unit_qty_convert.convertTo('inch');
            expect(inches.value()).toEqual(1200);
        });


        it('throws error when passed a bad unit_identifier', function(){
            var feet             = unit_collection.get('feet');
            var unit_qty         = UnitQty(feet, 100);
            var unit_qty_convert = UnitQtyConvert(unit_qty, unit_conversion_collection);
            expect(unit_qty_convert.convertTo.bind(null, 'something')).toThrow();
        });
    });

});