define([
    'json!UnitsOfMeasure/definitions/length.json'
], function(
    length_definitions
){
    'use strict';

    describe('length_definitions', function(){

        it('is an object', function(){
            expect(typeof length_definitions).toEqual('object');
        });

        it('has units which is an object', function(){
            expect(length_definitions.units instanceof Object).toBe(true);
        });

        it('has conversions which is an array', function(){
            expect(length_definitions.conversions instanceof Array).toBe(true);
        });

        it('has expected values', function(){
            expect(length_definitions.units.foot.unit).toEqual('foot');
            expect(length_definitions.units.foot.plural).toEqual('feet');
            expect(length_definitions.units.foot.abbrev).toEqual('ft');
        });

    });

});