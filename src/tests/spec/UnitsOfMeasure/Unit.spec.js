define([
    'UnitsOfMeasure/Unit'
], function(
    Unit
){
    'use strict';

    describe('Unit', function(){

        var unit_definition =  {
            unit    : 'foot',
            plural  : 'feet',
            abbrev  : 'ft',
            other   : []
        };

        var unit;

        beforeEach(function(){
            unit = Unit(unit_definition);
        });

        it('is an object', function(){
            expect(typeof unit).toEqual('object');
        });

        it('has expected values', function(){
            expect(unit.property('unit')).toEqual('foot');
            expect(unit.property('plural')).toEqual('feet');
            expect(unit.property('abbrev')).toEqual('ft');
        });

        it('has matches', function(){
            expect(unit.matches('foot')).toBe(true);
            expect(unit.matches('feet')).toBe(true);
            expect(unit.matches('ft')).toBe(true);
            expect(unit.matches('fts')).toBe(false);
        });

    });

});