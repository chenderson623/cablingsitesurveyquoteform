define([
    'UnitsOfMeasure/UnitCollection',
    'UnitsOfMeasure/UnitConversionCollection'
], function(
    UnitCollection,
    UnitConversionCollection
){
    'use strict';

    describe('UnitConversionCollection', function(){

        var conversions = [
            [12 , 'inch'  , 1          , 'foot'],
            [1  , 'yard'  , 3          , 'foot'],
            [1  , 'mile'  , 5280       , 'foot'],
            [1  , 'meter' , 3.28083989 , 'foot']
        ];

        var unit_definitions = {
            'foot' : {
                'unit'    : 'foot',
                'plural'  : 'feet',
                'abbrev'  : 'ft',
                'other'   : []
            },
            'inch' : {
                'unit'    : 'inch',
                'plural'  : 'inches',
                'abbrev'  : 'in',
                'other'   : []
            },
            'yard' : {
                'unit'    : 'yard',
                'plural'  : 'yards',
                'abbrev'  : 'yd',
                'other'   : []
            },
            'mile' : {
                'unit'    : 'mile',
                'plural'  : 'miles',
                'abbrev'  : 'mi',
                'other'   : []
            },
            'meter' : {
                'unit'    : 'meter',
                'plural'  : 'meters',
                'abbrev'  : 'm',
                'other'   : ['mtr']
            }
        };

        var unit_collection;
        var unit_conversion_collection;

        beforeEach(function(){
            unit_collection            = UnitCollection(unit_definitions);
            unit_conversion_collection = UnitConversionCollection(unit_collection, conversions);
        });

        it('is a function', function(){
            expect(typeof UnitConversionCollection).toEqual('function');
        });

        it('returns an object', function(){
            expect(typeof UnitConversionCollection(unit_collection, conversions)).toEqual('object');
        });

        it('returns collection length', function(){
            expect(unit_conversion_collection.length()).toEqual(4);
        });

        it('can loop through collection', function(){

            var spys = {
                conversionHit: function() {

                }
            };

            spyOn(spys, 'conversionHit');

            unit_conversion_collection.each(spys.conversionHit);

            expect(spys.conversionHit).toHaveBeenCalledTimes(4);
        });

        it('can find a conversion', function(){
            expect(typeof unit_conversion_collection.findConversion(unit_collection.get('foot'), 'inch')).toEqual('object');
            expect(unit_conversion_collection.findConversion(unit_collection.get('foot'), 'inch').getConversionMultiplier('inch')).toEqual(12);
        });

        it('returns undefined if no conversion found', function(){
            expect(unit_conversion_collection.findConversion(unit_collection.get('foot'), 'something')).toEqual(undefined);
        });

    });
});