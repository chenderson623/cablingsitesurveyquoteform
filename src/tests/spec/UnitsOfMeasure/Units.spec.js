define([
    'UnitsOfMeasure/UnitCollection'
], function(
    UnitCollection
){
    'use strict';

    describe('UnitCollection', function(){

        var unit_definitions = {
            'foot' : {
                'unit'    : 'foot',
                'plural'  : 'feet',
                'abbrev'  : 'ft',
                'other'   : []
            },
            'inch' : {
                'unit'    : 'inch',
                'plural'  : 'inches',
                'abbrev'  : 'in',
                'other'   : []
            },
            'yard' : {
                'unit'    : 'yard',
                'plural'  : 'yards',
                'abbrev'  : 'yd',
                'other'   : []
            },
            'mile' : {
                'unit'    : 'mile',
                'plural'  : 'miles',
                'abbrev'  : 'mi',
                'other'   : []
            },
            'meter' : {
                'unit'    : 'meter',
                'plural'  : 'meters',
                'abbrev'  : 'm',
                'other'   : ['mtr']
            }
        };

        it('is a function', function(){
            expect(typeof UnitCollection).toEqual('function');
        });

        it('returns an object', function(){
            expect(typeof UnitCollection(unit_definitions)).toEqual('object');
        });

        it('can access unit definition', function(){
            expect(typeof UnitCollection(unit_definitions).foot).toEqual('object');
            expect(UnitCollection(unit_definitions).foot.property('plural')).toEqual('feet');
        });

        it('can access unit definition with get', function(){
            expect(typeof UnitCollection(unit_definitions).foot).toEqual('object');
            expect(UnitCollection(unit_definitions).get('foot').property('plural')).toEqual('feet');
        });

        it('can access unit definition matches', function(){
            expect(UnitCollection(unit_definitions).get('feet').property('plural')).toEqual('feet');
        });

    });

});