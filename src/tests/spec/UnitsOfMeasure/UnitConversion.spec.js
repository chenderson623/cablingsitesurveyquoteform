define([
    'UnitsOfMeasure/Unit',
    'UnitsOfMeasure/UnitConversion'
], function(
    Unit,
    UnitConversion
){
    'use strict';

    describe('UnitConversion', function(){

        var foot_definition =  {
            unit    : 'foot',
            plural  : 'feet',
            abbrev  : 'ft',
            other   : []
        };

        var inch_definition =  {
            unit    : 'inch',
            plural  : 'inches',
            abbrev  : 'in',
            other   : []
        };

        var conversion = [12 , Unit(inch_definition), 1, Unit(foot_definition)];

        var unit_conversion;

        beforeEach(function(){
            unit_conversion = UnitConversion.apply(null, conversion);
        });

        it('is an object', function(){
            expect(typeof unit_conversion).toEqual('object');
        });

        it('hasUnit works', function(){
            expect(unit_conversion.hasUnit('inch')).toEqual(true);
            expect(unit_conversion.hasUnit('foot')).toEqual(true);
            expect(unit_conversion.hasUnit('something')).toEqual(false);
        });

        it('getConversionMultiplier works', function(){
            expect(unit_conversion.getConversionMultiplier('inch')).toEqual(12);
            expect(unit_conversion.getConversionMultiplier('foot').toFixed(4)).toEqual('0.0833');
        });
    });

});