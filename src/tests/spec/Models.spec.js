define([
    'backbone',
    'Models/Address',
    'Models/SiteSurvey',
    'Models/DmarcExtension',
    'json!tests/data/address1.json',
    'json!tests/data/dmarc_extension.json',
    'json!tests/data/dmarc_circuits.json'
], function(
    Backbone,
    Address,
    SiteSurvey,
    DmarcExtension,
    address1,
    dmarc_extension,
    dmarc_circuits
){
    'use strict';

    describe('Models', function(){
        describe('Address', function(){

            it('is associated model', function(){
                var address = new Address.Model();
                expect(address instanceof Backbone.AssociatedModel).toBe(true);
            });

            it('is fillable', function(){
                var address = new Address.Model(address1);
                expect(address.get('street_address')).toEqual('601 SPACE PARK NORTH');
            });

        });


        describe('SiteSurvey', function(){

            it('is associated model', function(){
                var site_survey = new SiteSurvey.Model();
                expect(site_survey instanceof Backbone.AssociatedModel).toBe(true);
            });

            it('has a dmarc extension collection', function(){
                var site_survey = new SiteSurvey.Model();
                var dmarc_extension_collection = site_survey.get('demarc_extensions');
                expect(dmarc_extension_collection instanceof Backbone.Collection).toBe(true);
            });

            it('can create a dmarc extension model', function(){
                var site_survey = new SiteSurvey.Model();
                var dmarc_extension_collection = site_survey.get('demarc_extensions');
// THis is an Assiciation object:
console.log(dmarc_extension_collection);

var dmarc_extension = new DmarcExtension.Model(dmarc_extension);
console.log(dmarc_extension);
                // TODO: how to add models
                //var dmarc_extension = dmarc_extension_collection.add(new DmarcExtension.Model(dmarc_extension));
                //console.log(dmarc_extension);
                //expect(dmarc_extension instanceof DmarcExtension).toBe(true);
                //TODO: redo these:
                /*
                var dmarc_extension = dmarc_extension_collection.add(dmarc_extensions[0]);
                expect(dmarc_extension instanceof Backbone.AssociatedModel).toBe(true);
                expect(dmarc_extension instanceof DmarcExtension.Model).toBe(true);
                expect(dmarc_extension.get('circuit_id')).toEqual('circuit1');
                // can also access as:
                expect(site_survey.get('demarc_extensions[0].circuit_id')).toEqual('circuit1');
                */
            });

        });

    });

});