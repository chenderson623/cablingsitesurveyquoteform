define([
    'backbone',
    'Models/DmarcExtension',
    'Models/DmarcCircuit',
    'Models/CableDrop',
    'json!tests/data/dmarc_extension.json',
    'json!tests/data/dmarc_circuits.json',
    'json!tests/data/cable_drops.json'
], function(
    Backbone,
    DmarcExtension,
    DmarcCircuit,
    CableDrop,
    dmarc_extension,
    dmarc_circuits,
    cable_drops
){
    'use strict';

    describe('Models/DmarcExtension', function(){

        it('is associated model', function(){
            var dmarc_extension = new DmarcExtension.Model();
            expect(dmarc_extension instanceof Backbone.AssociatedModel).toBe(true);
        });

        it('can add circuit from data', function(){
            var dmarc_extension = new DmarcExtension.Model();
            var dmarc_circuit   = dmarc_extension.get('dmarc_circuits').add(dmarc_circuits[0]);
            expect(dmarc_circuit instanceof Backbone.Model).toBe(true);
            expect(dmarc_circuit instanceof DmarcCircuit.Model).toBe(true);
        });

        it('can add circuit from full graph', function(){
            var data = dmarc_extension;
            dmarc_extension.dmarc_circuits = dmarc_circuits;
            var dmarc_extension_model     = new DmarcExtension.Model(data);
            var dmarc_circuits_collection = dmarc_extension_model.get('dmarc_circuits');
            expect(dmarc_circuits_collection instanceof Backbone.Collection).toBe(true);
            expect(dmarc_circuits_collection.length).toEqual(2);
        });

        it('can add cable drop from data', function(){
            var dmarc_extension = new DmarcExtension.Model();
            var cable_drop      = dmarc_extension.get('cable_drops').add(cable_drops[0]);
            expect(cable_drop instanceof Backbone.Model).toBe(true);
            expect(cable_drop instanceof CableDrop.Model).toBe(true);
        });

        it('adding cable drop triggers event', function(){
            var dmarc_extension = new DmarcExtension.Model();
            Backbone.Associations.EVENTS_NC = true;

            var spys = {
                cableDropAdded: function() {
                    console.log("cableDropAdded", arguments);
                }
            };

            spyOn(spys, 'cableDropAdded').and.callThrough();

            dmarc_extension.get('cable_drops').on('add', spys.cableDropAdded);

            dmarc_extension.get('cable_drops').add(cable_drops[0]);

            expect(spys.cableDropAdded).toHaveBeenCalled();
        });

        /* doesn't work? it('adding cable drop triggers event', function(){
            var dmarc_extension = new DmarcExtension.Model();
            Backbone.Associations.EVENTS_NC = true;

            var spys = {
                cableDropAdded: function() {
                    console.log("cableDropAdded", arguments);
                }
            };

            spyOn(spys, 'cableDropAdded').and.callThrough();

            dmarc_extension.on('nested-change', function(){console.log('NESTEDCHANGE');});
            dmarc_extension.on('nested-change', spys.cableDropAdded);
//            dmarc_extension.get('cable_drops').on('add', spys.cableDropAdded);

            var cable_drop = dmarc_extension.get('cable_drops').add(cable_drops[0]);

            expect(spys.cableDropAdded).toHaveBeenCalled();
        });
        */

    });

        it('changing cable drop triggers event', function(){
            var dmarc_extension = new DmarcExtension.Model();
            Backbone.Associations.EVENTS_NC = true;

            var spys = {
                cableDropChanged: function() {
                    console.log("cableDropChanged", arguments);
                }
            };

            spyOn(spys, 'cableDropChanged').and.callThrough();

            dmarc_extension.get('cable_drops').on('change', spys.cableDropChanged);

            dmarc_extension.get('cable_drops').add(cable_drops[0]);
            dmarc_extension.get('cable_drops[0]').set('cable_length', 300);
            expect(spys.cableDropChanged).toHaveBeenCalled();
        });

});