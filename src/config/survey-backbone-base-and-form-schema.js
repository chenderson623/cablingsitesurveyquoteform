define([
    'underscore',
    'json!data/states.json',
    'json!config/survey-form.json'
], function(
    _,
    states,
    survey_settings
) {
    'use strict';

    return {
        location: {
            name: {
                type: 'Text',
                options: {
                    label: 'Location Name'
                }
            },
        },
        address: {
            street_address: {
                type: 'Text',
                options: {
                    label: 'Address'
                }
            },
            street_address2: {
                type: 'Text',
                options: {
                    label: 'Address Line 2'
                }
            },
            suite: {
                type: 'Text',
                options: {
                    label: 'Apt / Suite / Other'
                }
            },
            city: {
                type: 'Text',
                options: {
                    label: 'City'
                }
            },
            state: {
                type: 'Select',
                options: {
                    label: 'State',
                    possibleVals: _.extend({'': ''}, states)
                }
            },
            zip_code: {
                type: 'Text',
                options: {
                    label: 'Zip Code'
                }
            },
        },
        dmarc_extension: {
            circuit_type: {
                type: 'Select',
                options: {
                    label: 'Circuit Type To Extend',
                    possibleVals: [''].concat(survey_settings.circuit_types, ['Other'])
                }
            },
            circuit_type_other: {
                type: 'Text',
                options: {
                    label: 'Circuit Type To Extend (Other)',
                }
            },
            dmarc_location: {
                type: 'Text',
                options: {
                    elementType: 'textarea',
                    label: 'Detailed DMARC Location Info',
                }
            },
            dmarc_circuits_tagged: {
                type: 'YesNo',
                options: {
                    label: 'Are Circuits Tagged At DMARC',
                }
            },
            dmarc_circuits_qty: {
                type: 'Text',
                options: {
                    label: 'How Many Extension Are Required',
                }
            },
        },
        dmarc_circuit: {
            ilec_circuit_id: {
                type: 'Text',
                options: {
                    label: 'ILEC CircuitID',
                }
            },
            binding_post: {
                type: 'Text',
                options: {
                    label: 'Binding Post',
                }
            },
            extend_to: {
                type: 'Text',
                options: {
                    label: 'Circuit Need Extend To'
                }
            }
        },
        cable_drops: {
            cable_drops_qty: {
                type: 'Text',
                options: {
                    label: 'How Many Cable Drops',
                }
            }
        },
        cable_drop: {
            type_of_cable: {
                type: 'Select',
                options: {
                    label: 'Type Of Cable Required',
                    possibleVals: [''].concat(survey_settings.cable_types, ['Other'])
                }
            },
            type_of_cable_other: {
                type: 'Text',
                options: {
                    label: 'Type Of Cable Required (Other)'
                }
            },
            cable_length: {
                type: 'Number',
                options: {
                    label: 'Cable Length (in feet)'
                }
            },
            conduit_required: {
                type: 'YesNo',
                options: {
                    label: 'Will Conduit Be Required'
                }
            },
            estimated_time_needed: {
                type: 'Decimal',
                options: {
                    label: 'Esimated Time Needed (hours)'
                }
            },
            describe_cable_path: {
                type: 'Text',
                options: {
                    label: 'Describe Cable Path That Will Be Used'
                }
            }
        }
    };
});
