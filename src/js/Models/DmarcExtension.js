define([
    'common/extend-backbone-with-backbone-associations',
    'Models/DmarcCircuit',
    'Models/CableDrop'
], function(
    Backbone,
    DmarcCircuit,
    CableDrop
) {
    'use strict';

    var Model = Backbone.AssociatedModel.extend({
        defaults: {
            circuit_type          : null,
            circuit_type_other    : null,
            dmarc_location        : null,
            dmarc_circuits_tagged : null,
            dmarc_circuits_qty    : 0,
            dmarc_circuits        : [],
            cable_drops           : []
        },
        relations: [
            {
                key: 'dmarc_circuits',
                collectionType: DmarcCircuit.Collection,
                type: Backbone.Many
            },
            {
                key: 'cable_drops',
                collectionType: CableDrop.Collection,
                type: Backbone.Many
            }
        ]
    });

    var Collection =  Backbone.Collection.extend({
        model: Model
    });

    return {
        Model: Model,
        Collection: Collection
    };
});
