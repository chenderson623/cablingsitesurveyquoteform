define([
    'common/extend-backbone-with-backbone-associations'
], function(
    Backbone
) {
    'use strict';

    var Model = Backbone.AssociatedModel.extend({
        defaults: {
            type_of_cable: '',
            type_of_cable_other: '',
            cable_length: 0,
            conduit_required: 'No',
            estimated_time_needed: 0,
            describe_cable_path: ''
        }
    });

    var Collection =  Backbone.Collection.extend({
        model: Model
    });

    return {
        Model: Model,
        Collection: Collection
    };
});
