define([
    'common/extend-backbone-with-backbone-associations'
], function(
    Backbone
) {
    'use strict';

    var Model = Backbone.Model.extend({
        defaults: {
            ilec_circuit_id : null,
            binding_post    : null,
            extend_to       : null
        },
    });

    var Collection =  Backbone.Collection.extend({
        model: Model
    });

    return {
        Model: Model,
        Collection: Collection
    };
});
