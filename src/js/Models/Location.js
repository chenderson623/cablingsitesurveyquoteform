define([
    'common/extend-backbone-with-backbone-associations',
    'Models/Address',
], function(
    Backbone,
    Address
) {
    'use strict';

    var Model = Backbone.AssociatedModel.extend({
        defaults: {
            name: ''
        },
        relations: [{
            key: 'address',
            relatedModel: Address.Model,
            type: Backbone.One
        }]
    });

    var Collection =  Backbone.Collection.extend({
        model: Model
    });

    return {
        Model: Model,
        Collection: Collection
    };
});
