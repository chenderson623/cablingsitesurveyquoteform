define([
    'common/extend-backbone-with-backbone-associations',
    'Models/Location',
    'Models/DmarcExtension'
], function(
    Backbone,
    Location,
    DmarcExtension
) {
    'use strict';

    var Model = Backbone.AssociatedModel.extend({
        defaults: {
            location: {},
            demarc_extensions: []
        },
        relations: [{
            key: 'location',
            relatedModel: Location.Model,
            type: Backbone.One
        }, {
            key: 'demarc_extensions',
            collectionType: DmarcExtension.Collection,
            type: Backbone.Many
        }]
    });

    return {
        Model: Model
    };
});







