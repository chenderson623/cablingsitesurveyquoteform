define([
    'common/extend-backbone-with-backbone-associations'
], function(
    Backbone
) {
    'use strict';

    var Model = Backbone.AssociatedModel.extend({
    });

    var Collection =  Backbone.Collection.extend({
        model: Model
    });

    return {
        Model: Model,
        Collection: Collection
    };
});
