define([
    'common/extend-backbone-with-backbone-associations'
], function(
    Backbone
) {
    'use strict';

    var Model = Backbone.AssociatedModel.extend({
    });

    return {
        Model: Model
    };
});
