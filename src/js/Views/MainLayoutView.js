define([
    'underscore',
    'backbone',
    'jquery',
    'jquery.showLoading'
    //'jquery-ui' //needed for easeOutQuart
], function(
    _,
    Backbone,
    $
) {
    'use strict';

    //add custom easing - in case jqueryui not loaded
    $.easing.easeOutQuart = function(x, t, b, c, d) {
        t = t / d - 1;
        return -c * (Math.pow(t, 4) - 1) + b;
    };

    var showLoading = function showLoading($elem) {
        $elem.showLoading();
    };

    var hideLoading = function hideLoading() {
        $('.loading-indicator-overlay').remove();
        $('.loading-indicator').remove();
    };

    var fadeOut = function fadeOut($elem) {
        return $elem.animate({
            opacity: 0
        }, {
            duration: 600,
            easing: 'easeOutQuart'
        }).promise();
    };

    var fadeIn = function fadeIn($elem) {
        return $elem.animate({
            opacity: 1
        }, {
            duration: 600,
            easing: 'easeOutQuart'
        }).promise();
    };

    function removeOrDetach(view) {
        if (typeof view.detach === 'function') {
            view.detach(); //subview wants to be saved in memory
        } else {
            view.remove();
        }
    }

    /*
     *
     * --------------Main Layout-----------------------------
     *
     */
    var MainLayoutView = Backbone.View.extend({
        template: null,

        element_selectors: {
            paging_controls     : '.paging-controls-view',
            user_messages       : '.user-messages-view',
            controller_controls : '.controller-controls-view-container',
            controller_view     : '.controller-view-container'
        },

        sub_views: {
            paging_controls     : null,
            user_messages       : null,
            controller_controls : null,
            controller_view     : null
        },

        initialize: function(options) {
            if(options.template) {
                this.template = _.template(options.template);
            } else {
                throw 'Must pass template as an option.';
            }
        },

        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            this.trigger('rendered');
            return this;
        },

        //
        // 'Attached' views (see common/extend-backbone-view)
        // ----------------------------------------------------------------------------------
        setUserMessagesView: function(view) {
            this.setAssignedView('user_messages', view);
        },

        //
        // Paging Controls View
        // ----------------------------------------------------------------------------------
        setPagingControlsView: function(view) {
            this.setView('paging_controls', view);
        },

        //
        // Controller Controls View
        // ----------------------------------------------------------------------------------
        setControllerControlsView: function(view) {
            this.setView('controller_controls', view);
        },

        //
        // Controller View
        // ----------------------------------------------------------------------------------
        setControllerView: function(view) {
            this.setView('controller_view', view);
        },
        setPendingControllerView: function() {
            this.setPendingView('controller_view');

        },
        setControllerViewFade: function(view) {
            this.setViewWithFade('controller_view', view);
        },

        //
        // Methods
        // ----------------------------------------------------------------------------------
        setAssignedView: function(sub_view_key, view) {
            this.sub_views[sub_view_key] = view;
            this.assign(this.element_selectors[sub_view_key], view);
            this.makeViewAssigned(view);
        },

        setView: function(sub_view_key, view) {
            this.removeView(sub_view_key);
            this.sub_views[sub_view_key] = view;
            this.listenTo(view, 'rendered', hideLoading.bind(null, this.getSubViewElement(sub_view_key)));

            if(view.renderTo) {
                return view.renderTo(this.getSubViewElement(sub_view_key));
            }
            return this.getSubViewElement(sub_view_key).html(view.render().$el);
        },

        setViewWithFade: function(sub_view_key,view) {
            var $elem   = this.getSubViewElement(sub_view_key);
            var setView = this.setView.bind(this, sub_view_key,view);
            fadeOut($elem).done(function(){
                setView();
                fadeIn($elem);
                // trigger rendered in case this was a pre-existing view that won't be rendered again
                view.trigger('rendered');
            });
        },

        getSubViewElement: function(sub_view) {
            if(!this.element_selectors[sub_view]) {
                throw 'No selector for ' + sub_view;
            }
            return this.$(this.element_selectors[sub_view]);
        },

        _setViewRemoveFunc: function(view) {
            view.OLDremove = view.remove;
            // For attached views, we don't want the element destroyed
            view.remove    = function() {
                this.$el.empty();
                this.stopListening();
            };
        },

        _restoreViewRemoveFunc: function(view) {
            if (typeof view.OLDremove === 'function') {
                // was attached. don't want the view to keep a reference to element in this view
                view.el     = null;
                view.$el    = null;
                view.remove = view.OLDremove;
            }
        },

        makeViewAssigned: function(view) {
            if(typeof view.detach === 'function') {
                delete view.detach;
            }
            this._setViewRemoveFunc(view);
        },

        removeView: function(sub_view) {
            if (this.sub_views[sub_view]) {
                removeOrDetach(this.sub_views[sub_view]);
                this._restoreViewRemoveFunc(this.sub_views[sub_view]);

                this.sub_views[sub_view] = null;
            }
        },

        setPendingView: function(sub_view) {
            this.removeView(sub_view);
            showLoading(this.getSubViewElement(sub_view));
        },

    });

    return MainLayoutView;
});
