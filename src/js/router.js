define([
    'backbone'
], function(
    Backbone
) {
    'use strict';

    return function(App) {


        var MainRouter = Backbone.Router.extend({
            routes: {
                ''  : 'Home',
                '/' : 'Home',
            },
            Home: function() {
                App.debug.log('HOME');
            },
        });

        var Router = new MainRouter();

        Backbone.history.start({
            pushState: true,
            root: '/',
        });

        return Router;
    };
});