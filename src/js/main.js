/**
 *
 *  MAIN
 *
 */
require([
    'app',
    'router',
    'text!templates/main-layout.html',
    'Views/MainLayoutView'
], function(
    App,
    Router,
    layout_template,
    MainLayoutView
) {
    'use strict';

    //
    // ---------- Setup App ----------
    //
    App.bindSingleton('Router', function(){
        Router(App);
        App.trigger('Router:ready');
    });

    App.bindMakeable('MainLayoutView', function(){
        var layout_view = new MainLayoutView({
            el       : App.config.dom_container,  // Automatically attached to DOM
            model    : App.make('MainLayoutModel'),
            template : layout_template,
        });
        layout_view.on('rendered', function(){App.trigger('Layout:rendered');});
        return layout_view;
    });

    //
    // ---------- Initialize ----------
    //
    App.make('MainLayoutView').render();

    App.on('all', console.log.bind(console));

    App.make('Router');

});