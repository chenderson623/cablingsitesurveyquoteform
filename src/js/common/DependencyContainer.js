define([
    'underscore'
], function(
    _
) {
    'use strict';

    var DependencyContainer = function() {

        var container       = {};
        var binds           = {};
        var singleton_binds = {};

        var invokeMethod = function(obj, method, args) {
            if(_.isUndefined(obj[method])) {
                console.log('Invoke error', obj, method);
                throw method + ' does not exist';
            }
            obj[method].apply(obj, args);
        };

        //
        // ---------- Dependency Injection Functions -----------
        //

        var get = function(key, default_value) {
            if( _.has(container, key)) {
                return container[key];
            }
            if(_.isUndefined(default_value)) {
                return null;
            }
            return default_value;
        };

        var set = function(key, value) {
            container[key] = value;
        };

        var bindMakeable = function(name, closure) {
            binds[name] = closure;
        };

        var bindSingleton = function(name, closure) {
            if( _.has(container, name)) {
                throw name + ' has already been envoked';
            }
            singleton_binds[name] = closure;
        };

        var make = function(name, args) {
            if( _.has(container, name)) {
                return container[name];
            }
            if( _.has(binds, name)) {
                return binds[name].apply(null, args);
            }
            if( _.has(singleton_binds, name)) {
                container[name] = singleton_binds[name].apply(null, args);
                return container[name];
            }
            throw 'make() error: ' + name + ' is undefined';
        };

        var invoke = function(name, method, args) {
            var obj = make(name);
            if(obj instanceof Promise) {
                obj.then(function(obj){
                    invokeMethod(obj, method, args);
                });
                return;
            }
            invokeMethod(obj, method, args);
        };

        var load = function(name) {
            // handles a promise. Always returns a promise
            var obj = make(name);
            if(obj instanceof Promise) {
                return obj;
            }
            return Promise.resolve(obj);
        };

        return {
            get           : get,
            set           : set,
            bindMakeable  : bindMakeable,
            bindSingleton : bindSingleton,
            make          : make,
            invoke        : invoke,
            load          : load
        };

    };

    return {
        createContext: DependencyContainer
    };
});