define([
    'underscore',
    'backbone',
    'jquery',
], function(
    _,
    Backbone,
    $
) {
    'use strict';

    /**
     * Implement Backbone.View that generates the view from config
     * Uses a Collection and Model to trigger tab activations
     * Module hijacks the click from bootstrap tabs and triggers tab activation through model:active change
     * All view classes are overrideable with Module.options
     */

    var BootstrapTabView = Backbone.View.extend({
        tagName: 'li',

        initialize: function(){
            this.model.on('change:active', function(model, value){
                if(value === true) {
                    // let bootstrap do the work
                    this.$a.tab('show');
                }
            }, this);
        },

        renderAnchor: function() {
            this.$a = $('<a></a>').html(this.model.get('title'));
            this.$a.attr({
                href: '#' + this.model.get('target'),
                'data-toggle': 'tab',
                'aria-controls': this.model.get('target')
            });
            if (this.model.has('linkClass')) {
                this.$a.addClass(this.model.get('linkClass'));
            }

            return this.$a;
        },
        render: function() {
            this.$el.empty();

            if (this.model.has('itemClass')) {
                this.$el.addClass(this.model.get('itemClass'));
            }

            if(this.model.get('active') === true) {
                this.$el.addClass('active');
            }

            if (this.model.has('iconClass')) {
                var $icon = $('<i></i>').addClass(this.model.get('iconClass'));
                this.$el.append($icon);
            }

            this.$el.append(this.renderAnchor());
            return this;
        },
    });

    /**
     * Render a tab view as a UL compatible with Bootstrap. The end result is
     * <ul>
     *     <li><a href=""><i></i>Text</a></li>
     *     ...
     * </ul>
     *
     * The backbone collection will consist of models containing attributes title, linkClass, iconClass, itemClass, target
     * target is
     *
     * @type {*}
     */
    var BootstrapTabsView = Backbone.View.extend({
        tagName: 'ul',
        className: 'nav nav-tabs',

        initialize: function(options) {
            this.options = _.extend({
                tab_view: BootstrapTabView
            }, options);
        },

        render: function() {
            this.$el.empty();
            this.$el.attr('role', 'tablist');
            this.collection.each(function(model) { // (model, i)
                this.addTab(model);
            }, this);
            return this;
        },

        addTab: function(model) {
            var tab_view = new this.options.tab_view({model: model});
            this.$el.append(tab_view.render().$el);
        },

        detach: function() {
            this.$el.detach();
        }

    });

    var BootstrapPanelView = Backbone.View.extend({
        tagName: 'div',
        className: 'tab-pane',
        render: function() {
            this.$el.empty();
            this.$el.append(this.model.get('content'));
            // if a target has been specified then use that as the id
            this.$el.attr('id', this.model.get('target'));

            if(this.model.get('active') === true) {
                this.$el.addClass('active');
            }

            return this;
        }
    });

    var BootstrapPanelsView = Backbone.View.extend({
        tagName: 'div',
        className: 'tab-content',

        initialize: function(options) {
            this.options = _.extend({
                panel_view: BootstrapPanelView
            }, options);
        },

        render: function() {
            this.$el.empty();
            this.collection.each(function(model) { // (model, i)
                this.addPanel(model);
            }, this);
            return this;
        },

        addPanel: function(model) {
            var panel_view = new this.options.panel_view({model: model});
            this.$el.append(panel_view.render().$el);
        }
    });

    var ContainerView = Backbone.View.extend({
        tagName: 'div',
        className: 'tabs-wrap',
        initialize: function(options) {
            this.tabs_view = options.tabs_view;
            this.panels_view = options.panels_view;
        },
        render: function() {
            this.$el.empty();
            this.$el.append(this.tabs_view.render().el);
            this.$el.append(this.panels_view.render().el);
            return this;
        },
        detach: function() {
            this.$el.detach();
        }
    });

    function Module(tabsList, options) {
        this.options = $.extend({
            handle_click: true,
            tab_model:    Backbone.Model.extend(),
            tabs_view:    BootstrapTabsView,
            tab_view:     BootstrapTabView,
            panels_view:  BootstrapPanelsView,
            panel_view:   BootstrapPanelView
        }, options);

        if(!$.fn.tab) {
            throw 'This module requires bootstrap tab';
        }

        var TabsCollection = Backbone.Collection.extend({
            model: this.options.tab_model,
            modelId: function(attrs) {
                return attrs.target;
            }
        });

        this.tabs_collection = new TabsCollection();
        this._fillCollection(tabsList);

        this.container_view = null;
        this.tabs_view   = null;
        this.panels_view = null;
    }

    Module.prototype = _.extend(Backbone.Events, {
        _fillCollection: function(tabsList) {
            // create/add models and make sure one is active
            var active = null;
            _.each(tabsList, function(tab){
                var model = this._addModel(tab);
                // if one already active, deactivate
                if(model.get('active') === true && active) {
                    model.set('active', false);
                }
                if(model.get('active') === true && !active) {
                    active = model;
                }
            }, this);

            if (!active) {
                this.tabs_collection.at(0).set('active', true);
            }
        },
        _filterTarget: function(tab) {
            if(!tab.target) {
                tab.target = this.tabs_collection.cid + '_' + this.tabs_collection.size();
            }
            //remove #'s and spaces'
            tab.target = tab.target.replace(/# /, '');
        },
        _addModel: function(tab) {
            this._filterTarget(tab);
            return this.tabs_collection.add(tab);
        },
        _clearActive: function() {
            this.tabs_collection.each(function(model){
                model.set('active', false);
            });
        },
        _handleClick: function(evt) {
            // Stop all propagation. This will stop bootstrap $.fn.tab from firing
            evt.preventDefault();
            evt.stopPropagation();
            var hash   = $(evt.currentTarget).attr('href');
            var target = hash.replace(/#/, '');

            // blur rught away
            $(evt.currentTarget).blur();

            this.trigger('navigate', hash, target);
            if(this.options.handle_click) {
                this.activate.call(this, [target]);
            }
        },
        getTabsCollection: function() {
            return this.tabs_collection;
        },
        getTabsView: function() {
            if (this.tabs_view === null) {
                this.tabs_view = new this.options.tabs_view({
                    collection: this.tabs_collection,
                    events: {
                        'click a': this._handleClick.bind(this)
                    },
                    tab_view: this.options.tab_view
                });
            }
            return this.tabs_view;
        },
        getPanelsView: function() {
            if (this.panels_view === null) {
                this.panels_view = new this.options.panels_view({
                    collection: this.tabs_collection,
                    panel_view: this.options.panel_view
                }, this);
            }
            return this.panels_view;
        },

        render: function() {
            if(this.container_view === null) {
                this.container_view = new ContainerView({
                    tabs_view: this.getTabsView(),
                    panels_view: this.getPanelsView()
                });
                return this.container_view.render();
            }
            return this.container_view; // no need to re-render
        },

        rerender: function() {
            this.getTabsView.render();
            this.getPanelsView.render();
        },

        activate: function(target) {
            this._clearActive();
            if(this.tabs_collection.get(target)) {
                this.tabs_collection.get(target).set('active', true);
            }
        },

        addTab: function(tab) {
            var model = this._addModel(tab);
            this.getTabsView().addTab(model);
            this.getPanelsView().addPanel(model);
            if(model.get('active') === true) {
                this.activate(model.get('target'));
            }
        },

        detach: function() {
            this.container_view.detach();
        }

    });

    return Module;
});