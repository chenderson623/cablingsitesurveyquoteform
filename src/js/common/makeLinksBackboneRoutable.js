define([
    'backbone'
], function(Backbone){
    'use strict';

    // All navigation that is relative should be passed through the navigate
    // method, to be processed by the router. If the link has a `data-bypass`
    // attribute, bypass the delegation completely.
    //
    // app_root will most always be '/'
    return function($elem, app_root) {
        app_root = app_root || '/';
        $elem    = $elem || $(document);

        $elem.on('click', 'a[href]:not([data-bypass])', function(evt) {
            // Get the absolute anchor href.
            var href = {
                prop: $(this).prop('href'),
                attr: $(this).attr('href')
            };

            //if its an empty href, skip
            if (href.attr === '') {
                evt.preventDefault();
                return;
            }

            // Get the absolute root.
            var root = location.protocol + '//' + location.host + app_root;
            // Ensure the root is part of the anchor href, meaning it's relative.
            if (href.prop.slice(0, root.length) === root) {
                // Stop the default event to ensure the link will not cause a page
                // refresh.
                evt.preventDefault();

                // `Backbone.history.navigate` is sufficient for all Routers and will
                // trigger the correct events. The Router's internal `navigate` method
                // calls this anyways.  The fragment is sliced from the root.
                Backbone.history.navigate(href.attr, true);
            }
        });
    };

});