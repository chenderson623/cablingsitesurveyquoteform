define([
    'app-config',
    'common/DependencyContainer',
    'backbone',
    'underscore',
    'localstorage'
], function(
    app_config,
    DependencyContainer,
    Backbone,
    _
) {
    'use strict';

    /**
     * App is simply a combination of a dependency container + event emitter
     *
     */
    var App = _.extend({}, app_config,
        Backbone.Events,
        DependencyContainer.createContext()
    );

    App.debug = {
        log: function() {
            if(App.config.debug) {
                console.log.apply(console,arguments);
            }
        }
    };

    App.bindMakeable('MainLayoutModel', function(){
        return new Backbone.Model({title: App.config.title});
    });

    App.bindSingleton('LocalStorage', function(){
        var LocalStorageModel = Backbone.Model.extend({});

        var LocalStorage = Backbone.Collection.extend({
            // Reference to this collection's model.
            model: LocalStorageModel,
            // Save all of the datatable states under the `"datatable-states"` namespace.
            localStorage: new Backbone.LocalStorage(App.config.local_storage_namespace)
        });

        var local_storage = new LocalStorage();
        local_storage.fetch();
        return local_storage;
    });

    App.bindMakeable('LocalStorageModel', function(model_name){
        return App.make('LocalStorage').get(model_name) || App.make('LocalStorage').create({id: model_name});
    });

    return App;
});