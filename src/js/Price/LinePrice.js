define([
    'backbone',
    'underscore',
    'UnitsOfMeasure/UnitQty',
    'UnitsOfMeasure/UnitQtyConvert'
], function(
    Backbone,
    _,
    UnitQty,
    UnitQtyConvert
){
    'use strict';

    function typeofUnitQty(unit_qty) {
        return (typeof unit_qty.value === 'function' &&
            typeof unit_qty.unitString === 'function' &&
            typeof unit_qty.unit === 'function');
    }

    function typeofPricePerUnit(price_per_unit) {
        return (typeof price_per_unit.price === 'function' &&
            typeof price_per_unit.unitString === 'function' &&
            typeof price_per_unit.unit === 'function');
    }

    function getUnitQty(qty, price_per_unit) {
        if(typeof qty === 'undefined') {
            throw 'must pass qty';
        }
        if(typeof qty === 'number') {
            return UnitQty(price_per_unit.unit(), qty);
        }
        if(!typeofUnitQty(qty)) {
            throw 'Not a valid unit_qty';
        }
        //TODO check that UnitQty can convert to PricePerUnit
        return qty;
    }

    return function LinePrice(price_per_unit, unit_qty) {

        if(typeof unit_qty === 'undefined') {
            unit_qty = UnitQty(price_per_unit.unit(), 0);
        }

        return _.extend({}, Backbone.Events, {
            setQty: function(qty){ // qty is UnitQty OR number (assumes same Units as PricePerUnit)
                unit_qty = getUnitQty(qty, price_per_unit);
                this.trigger('change', [unit_qty]);
            },
            convertedUnitQty: function() {
                if(typeof unit_qty.convertTo !== 'function') {
                    throw 'must use a unit_qty object with a convertTo function';
                }
                return unit_qty.convertTo(price_per_unit.unit());
            },
            convertedQty: function() {return this.convertedUnitQty().value();},
            price: function(){
                return price_per_unit.unitPrice() * this.convertedQty();
            }
        });
    };
});

