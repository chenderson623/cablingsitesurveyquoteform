define([
], function(
){
    'use strict';

    var currency_symbol = '$';

    function unitValueString(unit_qty) {
        return (unit_qty.value() === 1) ? unit_qty.unit().property('abbrev') : unit_qty.string('abbrev');
    }

    function priceString(price) {
        return price.toFixed(2);
    }

    function string(price, unit_qty) {
        return currency_symbol + priceString(price) + ' per ' + unitValueString(unit_qty);
    }

    return function PricePerUnit(price, unit_qty) {

        if(typeof price !== 'number') {
            throw 'price must be a number';
        }
        if(typeof unit_qty.value !== 'function' || typeof unit_qty.unit !== 'function') {
            throw 'unit_qty must be a UnitQty object';
        }
        if(unit_qty.value() === 0) {
            throw 'unit_qty cannot be zero';
        }


        return {
            price      : function(){return price;},
            unitPrice  : function(){return this.price()/this.unitValue();},
            unit       : function(){return unit_qty.unit();},
            unitValue  : function(){return unit_qty.value();},
            string     : string.bind(null,  price, unit_qty)
        };
    };
});