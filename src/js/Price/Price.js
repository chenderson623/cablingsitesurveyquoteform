define([
    'Price/PricePerUnit',
    'UnitsOfMeasure/Length'
], function(
    PricePerUnit,
    Length
){
    'use strict';

    /**
     * A lexical shortcut to Units and PricePerUnit
     */

    function valueFromArguments(args) {
        if(args.length == 2) {
            if(typeof args[0] !== 'number') {
                throw 'if 2 arguments passed, the first should be the value';
            }
            return args[0];
        }
        return 1;
    }

    function unitIdentifierFromArguments(args) {
        if(args.length == 2) {
            if(typeof args[1] !== 'string') {
                throw 'if 2 arguments passed, the second should be the unit identifier';
            }
            return args[1];
        }
        if(typeof args[0] !== 'string') {
            throw 'if 1 argument passed, the first should be the unit identifier';
        }
        return args[0];
    }

    function validateUnitArguments(length_value, unit_identifier) {
        if(typeof length_value !== 'number') {
            throw 'value must be a number';
        }
        if(typeof unit_identifier !== 'string') {
            throw 'unit identifier must be a string';
        }
    }

    return function Price(price) {

        return {
            perLength: function(){  // arguments: (length_value, unit_identifier) OR (unit_identifier) - implies length_value = 1
                var length_value    = valueFromArguments(arguments);
                var unit_identifier = unitIdentifierFromArguments(arguments);
                validateUnitArguments(length_value, unit_identifier);
                return PricePerUnit(price, Length(length_value)(unit_identifier));
            },
            perFoot: function(){
                return PricePerUnit(price, Length(1)('foot'));
            }
        };
    };
});

