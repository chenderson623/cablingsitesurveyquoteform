define([
    'underscore',
    'UnitsOfMeasure/Unit'
], function(
    _,
    Unit
){
    'use strict';

    return function UnitCollection(unit_definitions) {
        var units =  _.mapObject(unit_definitions, function(unit_definition){
            return Unit(unit_definition);
        });

        units.get = function(unit_identifier){
            if(units[unit_identifier]) {
                return units[unit_identifier];
            }
            return _.find(units, function(unit){return unit.matches(unit_identifier);});
        };

        return units;
    };
});