define([
    'UnitsOfMeasure/UnitQty'
], function(
    UnitQty
){
    'use strict';

    function findConversion(unit_conversion_collection, unit_from, unit_identifier_to) {
        var conversion = unit_conversion_collection.findConversion(unit_from, unit_identifier_to);
        if(!conversion) {
            throw 'No conversion for "' + unit_identifier_to + '"';
        }
        return conversion;
    }

    function createNewUnitQty(value, multiplier, new_unit) {
        var new_value    = value * multiplier;
        var new_unit_qty = UnitQty(new_unit, new_value);
        return new_unit_qty;
    }

    function convertTo(unit_qty, unit_conversion_collection, unit_identifier) {
        if(typeof(unit_identifier) !== 'string' && typeof unit_identifier.string === 'function') {
            unit_identifier = unit_identifier.string();
        }
        var conversion   = findConversion(unit_conversion_collection, unit_qty.unit(), unit_identifier);
        var new_unit_qty = createNewUnitQty(unit_qty.value(), conversion.getConversionMultiplier(unit_identifier), conversion.getUnit(unit_identifier));

        return new_unit_qty;
    }

    return function UnitQtyConvert(unit_qty, unit_conversion_collection) {

        if(typeof unit_qty.value !== 'function' || typeof unit_qty.unit !== 'function') {
            throw 'unit_qty must be a UnitQty object';
        }
        if(typeof unit_conversion_collection.findConversion !== 'function') {
            throw 'unit_conversion_collection must be a UnitConversionCollection object';
        }

        return {
            convertTo: convertTo.bind(null, unit_qty, unit_conversion_collection)
        };
    };
});