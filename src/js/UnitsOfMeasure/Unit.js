define([
], function(
){
    'use strict';

    function getProperty(unit_definition, property_key) {
        if(!property_key) {
            return unit_definition;
        }
        if(typeof unit_definition[property_key] === 'undefined') {
            return null;
        }
        return unit_definition[property_key];
    }
    function getPlural(unit_definition) {
        return getProperty(unit_definition, 'plural');
    }
    function getUnit(unit_definition) {
        return getProperty(unit_definition, 'unit');
    }
    function getAbbrev(unit_definition) {
        return getProperty(unit_definition, 'abbrev');
    }
    function hasOther(unit_definition, other_identifier) {
        var other = getProperty(unit_definition, 'other');
        other = (other instanceof Array) ? other : [];
        for(var i = 0, len = other.length; i < len; i++) {
            if(other[i] === other_identifier) {
                return true;
            }
        }
        return false;
    }
    function matches(unit_definition, unit_identifier) {
        return getUnit(unit_definition) === unit_identifier ||
            getPlural(unit_definition) === unit_identifier ||
            getAbbrev(unit_definition) === unit_identifier ||
             hasOther(unit_definition, unit_identifier);
    }

    return function Unit(unit_definition) {
        if(typeof unit_definition.unit === 'undefined' ||
            typeof unit_definition.plural === 'undefined' ||
            typeof unit_definition.abbrev === 'undefined') {
            throw 'unit definition must be an object with at least the properties unit, plural, and abbrev';
        }

        return {
            property : getProperty.bind(null, unit_definition),
            matches  : matches.bind(null, unit_definition),
            string   : function(){return unit_definition.unit;},
        };
    };
});