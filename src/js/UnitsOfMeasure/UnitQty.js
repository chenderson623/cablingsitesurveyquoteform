define([
], function(
){
    'use strict';

    function isUnit(value) {
        return value === 1;
    }

    function getUnitOrPlural(value) {
        return (isUnit(value)) ? 'unit' : 'plural';
    }

    function string(unit, value, unit_property) {
        if(typeof unit_property === 'undefined') {
            unit_property = getUnitOrPlural(value);
        }
        var units_string = unit.property(unit_property);
        return value + ' ' + units_string;
    }


    return function UnitQty(unit, value) {
        if(typeof value !== 'number') {
            throw 'value must be a number';
        }
        if(typeof unit.string !== 'function' || typeof unit.property !== 'function') {
            throw 'unit must be a Unit object';
        }

        return {
            value      : function(){return value;},
            string     : string.bind(null, unit, value),
            abbrev     : string.bind(null, unit, value, 'abbrev'),
            unitString : unit.string,
            unit       : function(){return unit;},
        };
    };
});