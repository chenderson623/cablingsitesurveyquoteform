define([
], function(
){
    'use strict';

    function hasUnit(unit1, unit2, unit_identifier) {
        return unit1.matches(unit_identifier) || unit2.matches(unit_identifier);
    }

    function getConversionMultiplier(unit1_value, unit1, unit2_value, unit2, unit_identifier) {
        if(!hasUnit(unit1, unit2, unit_identifier)) {
            throw 'No conversion for unit_identifier: ' + unit_identifier;
        }
        if(unit1.matches(unit_identifier)) {
            return unit1_value / unit2_value;
        }
        return unit2_value / unit1_value;
    }

    function getUnit(unit1, unit2, unit_identifier) {
        return unit1.matches(unit_identifier) ? unit1 : (unit2.matches(unit_identifier) ? unit2 : null);
    }

    return function UnitConversion(unit1_value, unit1, unit2_value, unit2) {
        if(typeof unit1_value !== 'number') {
            throw 'unit1_value must be a number';
        }
        if(typeof unit1.matches !== 'function') {
            throw 'unit1 must be a Unit object';
        }
        if(typeof unit2_value !== 'number') {
            throw 'unit2_value must be a number';
        }
        if(typeof unit2.matches !== 'function') {
            throw 'unit2 must be a Unit object';
        }
        return {
            getConversionDefinition : function(){return [unit1_value, unit1, unit2_value, unit2];},
            hasUnit                 : hasUnit.bind(null, unit1, unit2),
            getConversionMultiplier : getConversionMultiplier.bind(null, unit1_value, unit1, unit2_value, unit2),
            getUnit                 : getUnit.bind(null, unit1, unit2)
        };
    };
});