define([
    'UnitsOfMeasure/LengthUnits',
    'UnitsOfMeasure/UnitQty',
    'UnitsOfMeasure/UnitQtyConvert'
], function(
    LengthUnits,
    UnitQty,
    UnitQtyConvert
){
    'use strict';

    return function(value) {

        return function(unit_identifier) {
            var length = UnitQty(LengthUnits.get(unit_identifier), value);
            length.convertTo = function(unit_identifier_to){
                return UnitQtyConvert(length, LengthUnits.conversions).convertTo(unit_identifier_to);
            };
            return length;
        };

    };

});