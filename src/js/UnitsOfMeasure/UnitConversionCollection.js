define([
    'underscore',
    'UnitsOfMeasure/UnitConversion'
], function(
    _,
    UnitConversion
){
    'use strict';

    function selfConversion(unit) {
        return UnitConversion(1, unit, 1, unit);
    }

    function findConversion(collection, unit_from, unit_identifier_to) {

        if(typeof unit_from.matches !== 'function') {
            throw 'unit_from must be a Unit object';
        }
        if(!_.isString(unit_identifier_to)) {
            throw 'unit_identifier_to must be a string';
        }

        if(unit_from.matches(unit_identifier_to)) {
            // trying to convert itself
            return selfConversion(unit_from);
        }
        return _.find(collection, function(unit_conversion){
            return unit_conversion.hasUnit(unit_identifier_to);
        });
    }

    function hasConversion(collection, unit, unit_identifier) {
        return findConversion(collection, unit, unit_identifier) !== undefined;
    }

    return function UnitConversionCollection(unit_collection, conversions) {

        if(!_.isArray(conversions)) {
            throw 'conversions must be an array';
        }
        if(typeof unit_collection.get !== 'function') {
            throw 'unit_collection must be a UnitCollection object';
        }

        var collection = _.map(conversions, function(conversion){
            return UnitConversion(conversion[0], unit_collection.get(conversion[1]), conversion[2], unit_collection.get(conversion[3]));
        });

        return {
            length         : function(){return collection.length;},
            each           : _.each.bind(null, collection),
            findConversion : findConversion.bind(null, collection),
            hasConversion  : hasConversion.bind(null, collection)
        };
    };
});