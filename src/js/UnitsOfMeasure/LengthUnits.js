define([
    'json!UnitsOfMeasure/definitions/length.json',
    'UnitsOfMeasure/UnitCollection',
    'UnitsOfMeasure/UnitConversionCollection'
], function(
    unit_definitions,
    UnitCollection,
    UnitConversionCollection
){
    'use strict';

    var LengthUnits = UnitCollection(unit_definitions.units);
    LengthUnits.conversions = UnitConversionCollection(LengthUnits, unit_definitions.conversions);

    return LengthUnits;
});