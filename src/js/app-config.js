define([], function(){
    'use strict';

    var app_config = window.app_config || {}; // this can be set as a global variable

    var die = function(message) {
        throw message;
    };

    return {
        config: {
            debug    : app_config.debug    || false,
            app_root : app_config.app_root || '',
            app_url  : app_config.app_url  || 'http://192.168.25.36:9001/',
            title    : app_config.title    || 'Cabling Site Survey Quote Form',

            local_storage_namespace : app_config.local_storage_namespace || 'cabling_site_survey_quote_form',

            dom_container : app_config.dom_container || die('Need to pass dom_container')
        }

    };
});