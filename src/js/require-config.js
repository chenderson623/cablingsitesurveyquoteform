require.config({
    baseUrl: '/js',
    urlArgs: 'bust=' + (new Date()).getTime(),
    paths: {
        'templates' : '../templates',
        'tests'     : '../tests',
        'data'      : '../data',
        'config'    : '../config',

        'jquery'     : '../bower_components/jquery/dist/jquery',
        'underscore' : '../bower_components/underscore/underscore-min',
        'backbone'   : '../bower_components/backbone/backbone',
        'text'       : '../bower_components/text/text',
        'json'       : '../bower_components/requirejs-plugins/src/json',

        'backbone-associations' : '../bower_components/backbone-associations/backbone-associations',

        'backbone-baseview' : '../lib/backbone-base-and-form-view/backbone-baseview',
        'backbone-formview' : '../lib/backbone-base-and-form-view/backbone-formview',

        'datatables.net'       : '../bower_components/datatables.net/js/jquery.dataTables.min',
        'datatables.net-bs'    : '../bower_components/datatables.net-bs/js/dataTables.bootstrap.min',
        'localstorage'         : '../bower_components/backbone.localStorage/backbone.localStorage-min',
        'jquery.showLoading'   : '../bower_components/jquery.showLoading/js/jquery.showLoading.min',
        'bootstrap-datepicker' : '../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
        'select2'              : '../bower_components/select2/dist/js/select2.min'
    },
    shim: {

    }
});