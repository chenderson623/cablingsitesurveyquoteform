define([
    'Models/Quote',
    'Domain/Quote/PricingModels',
    'json!config/survey-form.json'
], function(
    Quote,
    PricingModels,
    survey_form_config
){
    'use strict';

    // Returns a Quote Model
    return function(SiteSurvey) {

        var hourly_rate = new PricingModels.HourlyRate({
            price: survey_form_config.pricing.std.hourly_rate
        });

        // TODO: change these to Quote.listenTo?
        SiteSurvey.on('change:dmarc_extensions', function() {
            hourly_rate.set('quantity', SiteSurvey.dmarc_extensions.length);
        });

        var quote = new Quote.Model();

        return quote;
    };

});