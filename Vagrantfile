# -*- mode: ruby -*-
# vi: set ft=ruby :

#--------------------------------------------
# Vagrantfile
# Chris Henderson <chenderson623@gmail.com>
#
#--------------------------------------------

VAGRANTFILE_API_VERSION = "2"

# ---- VM Config ----
vm_config = {
  :box     => 'ubuntu/trusty64',
#  :box     => 'precise64',
  :box_url => ''
}

# ---- Machine Config ----
machine_config = {
  :machine_name => 'CablingSiteSurveyForm',
  :hostname     => 'cablingsitesurveyquoteform.app',
  :aliases      => [],

  :network      => {
    :type => 'private_network',
    :ip   => '192.168.25.37'
  },

  :synced_folders => [
    { :host => './', :guest => '/home/vagrant/cabling-form', :nfs => true, :mount_options => ['actimeo=1'] }
  ],

  # Provisioning settings:
  :timezone => "America/Phoenix"
}

# ---- Virtualbox Config ----
virtualbox_config = {
  :name         => File.basename(File.expand_path("../..",__FILE__)) + '--' + File.basename(File.expand_path("..",__FILE__)),  # This is the box name (the name virtualbox will have)
  :gui          => false,
  :linked_clone => true,
  :memory       => 1024,
  :cpus         => 2,
  :customize => {
    "--ioapic" => "on",

    #GUI stuff:
    #"--graphicscontroller" => "vboxvga",
    #"--accelerate3d" => "on",
    #"--vram" => "128",
    #"--hwvirtex" => "on",
    #"--clipboard" => "bidirectional",
    #"--draganddrop" => "bidirectional"
  }
}

#------------------------------------------------------------------
# Vagrant Run
#------------------------------------------------------------------
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # ---- Base Configurations ----
  config.vm.box     = vm_config[:box]
  config.vm.box_url = vm_config[:box_url]

  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'" #  -- prevent stdin is not a tty error

  # ---- Configure Provider(s) ----
  config.vm.provider :virtualbox do |vb|
    vb.name         = virtualbox_config[:name] if virtualbox_config.has_key?(:name)
    vb.gui          = virtualbox_config[:gui] || false
    vb.linked_clone = virtualbox_config[:linked_clone] || false  if Vagrant::VERSION =~ /^1.8/
    vb.memory       = virtualbox_config[:memory] || 1024
    vb.cpus         = virtualbox_config[:cpus] || 1
    virtualbox_config[:customize].each do |(param, value)|
      vb.customize ["modifyvm", :id, param, value]
    end
  end

  # ---- Define Machine ----
  config.vm.define machine_config[:machine_name]
  config.vm.hostname = machine_config[:hostname]

  #   -- Synced Folders --
  machine_config[:synced_folders].each do |synced_folder|
    if synced_folder[:nfs] && Vagrant::Util::Platform.windows?
      synced_folder[:nfs] = false
    end
    if synced_folder[:nfs]
      synced_folder[:mount_options] = synced_folder[:mount_options] || ['nolock,vers=3,udp,noatime']
    end
    config.vm.synced_folder synced_folder[:host], synced_folder[:guest],
      :nfs => synced_folder[:nfs],
      :mount_options => synced_folder[:mount_options],
      :create => synced_folder[:create] || false,
      :disabled => synced_folder[:disabled] || false
#      :owner => synced_folder[:owner] || "vagrant",
#      :group => synced_folder[:group] || "vagrant"
  end

  #   -- Network --
  config.vm.network machine_config[:network][:type], ip: machine_config[:network][:ip]
  #config.vm.network machine_config[:network][:type], type: "dhcp"

  # ---- Plugin Settings ----
  #   -- Cachier plugin --
  if Vagrant.has_plugin?("vagrant-cachier")
    puts "Using vagrant-cachier"
    config.cache.scope = :box
    if !Vagrant::Util::Platform.windows?
      config.cache.synced_folder_opts = {
          type: :nfs,
          mount_options: ['rw', 'vers=3', 'tcp', 'nolock']
      }
    end
  end

  #   -- Guest Additions plugin --
  if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.no_remote = true     # true = do NOT download the iso file from a webserver
    config.vbguest.auto_update = true   # false = you do NOT want to check the correct
    config.vbguest.no_install = false   # false = install the update. true = just check
  end

  #   -- Hostmanager plugin --
  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
    config.hostmanager.aliases = machine_config[:aliases]
  end


  # ---- Provisioning ----

  config.vm.provision "BASE",
    type: 'shell',
    privileged: true,
    keep_color: true,
    args: [machine_config[:timezone]],
    inline: <<SCRIPT

      set -e

      if [ -f "/home/vagrant/vagrant-base-provision-complete" ];then
        # Already run
        exit 0
      fi

      timezone=$1
      echo "#{$timezone}" | sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

      sudo apt-get -y update

      # adding noninteractive stuff so we're not prompted about grub
      unset UCF_FORCE_CONFFOLD
      export UCF_FORCE_CONFFNEW=YES
      ucf --purge /boot/grub/menu.lst

      export DEBIAN_FRONTEND=noninteractive
      apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade

      apt-get install -y python-software-properties zip unzip curl git vim mc

      apt-get autoremove -y

      touch /home/vagrant/vagrant-base-provision-complete

SCRIPT


  config.vm.provision "NODEJS",
    type: 'shell',
    privileged: false,
    keep_color: true,
    args: [],
    inline: <<SCRIPT

      # Exit immediately on error
      set -e

      # Detect whether output is piped or not.
      [[ -t 1 ]] && piped=0 || piped=1
      # Detect whether script is sourced or not.
      [[ "${BASH_SOURCE[0]}" != "${0}" ]] && sourced=1 || sourced=0

      die() { printf '%b\n' "$@"; exit 1; } >&2
      err() { printf '%b\n' " \033[1;31m✖\033[0m  $@"; } >&2
      success() { printf '%b\n' " \033[1;32m✔\033[0m  $@"; }
      log() { printf '%b\n' " \033[1;32m $@ \033[0m"; }

      function ubuntu_nodejs_nodesource {
        local VER="0.12"

        # TODO: check valid
        # valid options: "0.10","0.12","4.x","5.x","dev","iojs_1.x","iojs_2.x","iojs_3.x"

        local SCRIPT_URL="https://deb.nodesource.com/setup_$VER"

        hash node 2>/dev/null && installed=1 || installed=0
        if [ $installed -eq 0 ]; then
            log "----Installing Nodejs through $SCRIPT_URL----"
            curl -sL $SCRIPT_URL | sudo bash -
            sudo apt-get install -y nodejs
            success "Nodejs Installed"
        fi
      }

      function nodejs_nvm {
        if [ ! -e /home/vagrant/.nvm ];then
          curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
          source /home/vagrant/.nvm/nvm.sh
          nvm install 5
        fi
      }

      function npm_install_global {
        source /home/vagrant/.nvm/nvm.sh
        npm install -g $@
      }

      nodejs_nvm
      npm_install_global "grunt-cli bower"

      # phantomjs prerequisites:
      sudo apt-get install build-essential chrpath libssl-dev libxft-dev -y
      sudo apt-get install libfreetype6 libfreetype6-dev -y
      sudo apt-get install libfontconfig1 libfontconfig1-dev -y

      npm_install_global "phantomjs-prebuilt"

      success "DONE"


SCRIPT

  config.vm.provision "PROJECT",
    type: 'shell',
    privileged: false,
    keep_color: true,
    args: [],
    inline: <<SCRIPT

    # Exit immediately on error
    set -e

    # Detect whether output is piped or not.
    [[ -t 1 ]] && piped=0 || piped=1
    # Detect whether script is sourced or not.
    [[ "${BASH_SOURCE[0]}" != "${0}" ]] && sourced=1 || sourced=0

    echo "SOURCED $sourced"

    die() { printf '%b\n' "$@"; exit 1; } >&2
    err() { printf '%b\n' " \033[1;31m✖\033[0m  $@"; } >&2
    success() { printf '%b\n' " \033[1;32m✔\033[0m  $@"; }
    log() { printf '%b\n' " \033[1;32m $@ \033[0m"; }


    sudo apt-get install -y nginx

    nginx_copy_vhost() {
        local SOURCE=$1
        local DEST_FILENAME=$2

        sudo cp -rf $SOURCE /etc/nginx/sites-available/$DEST_FILENAME
        sudo chown root:root /etc/nginx/sites-available/$DEST_FILENAME
        sudo chmod 0644 /etc/nginx/sites-available/$DEST_FILENAME
    }

    nginx_enable_vhost() {
        local VHOST_FILENAME=$1
        if [ ! -e  /etc/nginx/sites-enabled/$VHOST_FILENAME ];then
            sudo ln -s /etc/nginx/sites-available/$VHOST_FILENAME /etc/nginx/sites-enabled/$VHOST_FILENAME
        fi
    }

    nginx_reload() {
        sudo service nginx restart
    }

    #nginx_copy_vhost /vagrant/testdocs.globalcapacitysync2.dev testdocs.globalcapacitysync2.dev
    #nginx_enable_vhost testdocs.globalcapacitysync2.dev

    #nginx_copy_vhost /vagrant/globalcapacitysync2.dev globalcapacitysync2.dev
    #nginx_enable_vhost globalcapacitysync2.dev

    #nginx_reload


SCRIPT


end
